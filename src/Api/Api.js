// Api.js
import {
    AsyncStorage,
    Alert
} from 'react-native';
import ApiUtils from './ApiUtils';

var API_ENDPOINT = 'http://votocast.com/apis/v4/';
let formdata = new FormData();

var fetchPostData = function(url, formdata, callback) {
  fetch(url, {
          method: 'POST',
          body: formdata
      })
      .then(ApiUtils.checkStatus)
      .then(response => response.json())
      .then((response) => {
          out = response;
          callback(undefined, out);
      })
      .catch(e => {
        console.log('-------------------> post data error ---> ' + e);
          callback(e);
      })
}

var Api = {
  formdata: new FormData(),

  doLogin: function(parent, username, password, callback) {
      url = API_ENDPOINT + "login";

      this.formdata = new FormData();
      this.formdata.append("username", username);
      this.formdata.append("password", password);

      console.log('--->>> login method --- ' + url + '\n--- ' + JSON.stringify(this.formdata));
      fetchPostData(url, this.formdata, function(err, data) {
          if (!err) {
              callback(parent, data);
          } else {
              console.log('login method error ---->> ' + err);
          }
      });
  },

};
module.exports = Api;
