/**
 * Login Screen
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  Button,
  Animated,Alert,
} from 'react-native';
import Api from '../Api/Api';

const appLogo = require( '../images/appLogo.png');
const loader = require( '../images/loader.png');

export default class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      email: "hiren43",
      password: "test1234"
    }
    this.spinValue = new Animated.Value(0);
  }

  spin() {
    this.spinValue.setValue(0);
    Animated.timing(
      this.spinValue, {
        toValue: 10,
        duration: 25000,
        useNativeDriver: true,
      }
    ).start();
  }

  loginPressed = () => {
    // check for email value
    if(this.state.email.length != 0){
      //check for password value
      if(this.state.password.length >= 8){
        //set loading animation
        this.setState({ isLoading : true });
        this.spin();
        const parent = this;
        // call login api
        Api.doLogin(this, this.state.email, this.state.password, function(parent, data){
            //check for api response error/success
            if(data.error == "1"){
              Alert.alert("Error",data.message);
              parent.setState({ isLoading : false });
            }else{
              setTimeout(function(){
                parent.setState({ isLoading : false });
                parent.props.navigation.navigate('Home')
              }, 500);
            }
        });
      }else{
        Alert.alert("Error","Please enter minimum 8 characters long password");
      }
    }else{
      Alert.alert("Error","Please enter valid email");
    }
  }

  handleEmail = (email: string) => {
    this.setState({ email : email });
  }

  handlePassword = (password: string) => {
    this.setState({ password : password });
  }

  render(){
    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
    });
    return (
      <View style= {{flex: 1}}>
        {this.state.isLoading ?
          <View style= {{flex: 1}}>
            <View style= {styles.loadingView}>
              <Animated.Image style={[styles.loadingAnimatedImage,{transform: [{rotate: spin}]}]} source={loader} />
            </View>
            <View style= {styles.loadingTextView}>
              <Text style= {styles.loadingText}> Loading... </Text>
            </View>
          </View>
        :
          <View style= {styles.container}>
            <Image source= {appLogo} style= {styles.logo} />
            <View style= {styles.form}>
              <TextInput
                style = {styles.textInput}
                value= {this.state.email}
                onChangeText= {this.handleEmail.bind()}
                placeholder= "Email"
                placeholderTextColor= "#898989"
                />
                <TextInput
                style = {styles.textInput}
                value= {this.state.password}
                onChangeText= {this.handlePassword.bind()}
                placeholder= "Password"
                placeholderTextColor= "#898989"
                secureTextEntry={true}
                />
                <Text style={styles.normalText}> Forgot {" "}
                <Text style={styles.clickableText} onPress={() => {}}>
                  Email
                  </Text>
                <Text style={styles.normalText}>
                  {" "} Or {" "}
                </Text>
                <Text style={styles.clickableText} onPress={() => {}}>
                  Password
                </Text>
                <Text> ? </Text>
              </Text>
              <TouchableOpacity onPress={this.loginPressed.bind()} style={styles.buttonStyle}>
                <Text style={styles.btnText}>
                  Log In
                </Text>
              </TouchableOpacity>
              <View style={styles.bottomView}>
                  <Text style={styles.normalText}> Don't Have An Account? {" "}
                    <Text
                      style={styles.clickableText}
                      onPress={() => {}}
                      >
                       Sign Up
                      </Text>
                  </Text>
              </View>
            </View>
          </View>
        }
      </View>
      );
    }
}

const styles = StyleSheet.create({
  loadingView:{
    flex: 0.6,
    justifyContent: "flex-end",
    alignItems: "center",
  },
  loadingAnimatedImage:{
    width: 105,
    height: 105,
    resizeMode:'contain',
  },
  loadingTextView:{
    flex: 0.4,
    marginTop:100,
    alignItems: "center"
  },
  loadingText:{
    position: "absolute",
    color: "#898989",
    fontFamily:'GothamRounded-Book'
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-between"
  },
  logo: {
    flex: 1,
    width: "50%",
    resizeMode: "contain",
    alignSelf: "center"
  },
  loader: {
    flex: 1,
    width: "33%",
    height: "33%",
    resizeMode: "contain",
    alignSelf: "center"
  },
  form: {
    flex: 1,
    width: "80%",
    justifyContent: "flex-start",
    fontFamily:'GothamRounded-Book'
  },
  textInput: {
    height: 40,
    marginBottom: 40,
    paddingLeft: 10,
    color:'#898989',
    backgroundColor: "#F6F7FB",
    fontFamily:'GothamRounded-Book',
    fontWeight:'500',
  },
  buttonStyle: {
    marginTop: 50,
    marginBottom: 30,
    height: 48,
    width: "100%",
    backgroundColor: '#31C5C3',
    borderRadius: 24,
    justifyContent: 'center',
    alignSelf: 'center',
    fontFamily:'GothamRounded-Book'
  },
  bottomView:{
    alignItems:'center'
  },
  normalText:{
    color: '#898989',
    fontFamily:'GothamRounded-Book',
    fontWeight:'500',
  },
  clickableText:{
    color: '#31C5C3',
    fontFamily:'GothamRounded-Book',
    fontWeight:'500',
  },
  btnText:{
    alignSelf: 'center',
    color: 'white',
    fontSize:16,
    fontFamily:'GothamRounded-Book',
    fontWeight:'500',
  },
});
