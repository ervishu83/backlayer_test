/**
 * Dashboard Screen
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  Image,
  TextInput,
  Dimensions,
  Button,
  FlatList,
} from 'react-native';

const appLogo = require( '../images/appLogo.png');
const help = require( '../images/help.png');
const bell = require( '../images/bell.png');
const search = require( '../images/search.png');
const searchWhite = require( '../images/search_white.png');

const imgA = require( '../images/A.png');
const imgASelected = require( '../images/A_selected.png');
const imgB = require( '../images/B.png');
const imgBSelected = require( '../images/B_selected.png');
const imgC = require( '../images/C.png');
const imgCSelected = require( '../images/C_selected.png');

const response = require( '../json/test.json');

var array1, array2, array3, arrayAll, selectedCatArray, arrayCategoryKeys;
var totalBalance = 0, selectedCat = 'all';

export default class Home extends Component {

  constructor(props) {
    super(props);
    const arrResponse = response.categories;
    arrayCategoryKeys = Object.keys(arrResponse);
    array1 = arrResponse[arrayCategoryKeys[0]]
    array2 = arrResponse[arrayCategoryKeys[1]]
    array3 = arrResponse[arrayCategoryKeys[2]]
    var arr = array1.concat(array2);
    arrayAll = arr.concat(array3);
    // total balance calculation
    arrayAll.map((item, i) => (
        totalBalance = totalBalance + item.balance
    ))
    selectedCatArray = arrayAll;
    this.state = {
        dataSource: arrayAll,
        categoryIndex: 4,
        filterIndex: 4,
        selectedCellIndex: -1,
        isSearchMode: false,
        selectedCatImage:imgA,
        filter1Text:'FILTER 1',
        filter2Text:'FILTER 2',
        filter3Text:'FILTER 3',
        filter4Text:'ALL',
        search:'',
    }
  }

  filterData = (catIndex) => {
    var arr = [];
    // filter data as per selected type
    if(this.state.filterIndex == 4){
      arr = selectedCatArray;
    }
    else{
      // filter data on bye type
      if(this.state.filterIndex == 1){
        selectedCatArray.map((item, i) => (
            item.type == "buy" ? arr.push(item) : console.log(item.type)
        ))
      }
      // filter data on sell type
      else if(this.state.filterIndex == 2){
        selectedCatArray.map((item, i) => (
            item.type == "sell" ? arr.push(item) : console.log(item.type)
        ))
      }
      // filter data on transfer type
      else if(this.state.filterIndex == 3){
        selectedCatArray.map((item, i) => (
            item.type == "transfer" ? arr.push(item) : console.log(item.type)
        ))
      }
    }
    this.setState({
        dataSource: arr,
        categoryIndex: catIndex,
        selectedCellIndex: -1,
    })
  }

  catAllPressed = () => {
    totalBalance = 0;
    //total balance for all items
    arrayAll.map((item, i) => (
        totalBalance = totalBalance + item.balance
    ))
    //set all items to list
    selectedCatArray = arrayAll;
    this.setState({selectedCatImage:imgA})
    this.filterData(4);
    selectedCat = 'all';
  }

  catAPressed = () => {
    totalBalance = 0;
    //total balance for all A category
    array1.map((item, i) => (
        totalBalance = totalBalance + item.balance
    ))
    selectedCatArray = array1;
    //set A category items to list
    this.setState({selectedCatImage:imgA})
    this.filterData(1);
    selectedCat = 'A';
  }

  catBPressed = () => {
    totalBalance = 0;
    //total balance for all B category
    array2.map((item, i) => (
        totalBalance = totalBalance + item.balance
    ))
    selectedCatArray = array2;
    //set B category items to list
    this.setState({selectedCatImage:imgB})
    this.filterData(2);
    selectedCat = 'B';
  }

  catCPressed = () => {
    totalBalance = 0;
    //total balance for all C category
    array3.map((item, i) => (
        totalBalance = totalBalance + item.balance
    ))
    selectedCatArray = array3;
    //set C category items to list
    this.setState({selectedCatImage:imgC})
    this.filterData(3);
    selectedCat = 'C';
  }

  filterAllPressed = () => {
    var arr = [];
    //filter on list items
    selectedCatArray.map((item, i) => (
        arr.push(item)
    ))
    this.setState ({
        dataSource: arr,
        filterIndex: 4,
        selectedCellIndex: -1,
        filter1Text:'FILTER 1',
        filter2Text:'FILTER 2',
        filter3Text:'FILTER 3',
    })
  }

  filter1Pressed = () => {
    // Buy filter on list items
    var arr = [];
    selectedCatArray.map((item, i) => (
        item.type == "buy" ? arr.push(item) : console.log(item.type)
    ))
    this.setState ({
        dataSource: arr,
        filterIndex: 1,
        selectedCellIndex: -1,
        filter1Text:'BUY',
        filter2Text:'FILTER 2',
        filter3Text:'FILTER 3',
    })
  }

  filter2Pressed = () => {
    var arr = [];
    // Sell filter on list items
    selectedCatArray.map((item, i) => (
        item.type == "sell" ? arr.push(item) : console.log(item.type)
    ))
    this.setState ({
        dataSource: arr,
        filterIndex: 2,
        selectedCellIndex: -1,
        filter1Text:'FILTER 1',
        filter2Text:'SELL',
        filter3Text:'FILTER 3',
    })
  }

  filter3Pressed = () => {
    var arr = [];
    // Transfer filter on list items
    selectedCatArray.map((item, i) => (
        item.type == "transfer" ? arr.push(item) : console.log(item.type)
    ))
    this.setState ({
        dataSource: arr,
        filterIndex: 3,
        selectedCellIndex: -1,
        filter1Text:'FILTER 1',
        filter2Text:'FILTER 2',
        filter3Text:'TRANSFER',
    })
  }

  searchPressed = () => {
    this.setState ({
        isSearchMode: this.state.isSearchMode ? false : true, search:'',
    })
    this.handleSearch('');
  }

  handleCellSelection = (index) => {
    this.setState ({selectedCellIndex: index})
  }

  handleSearch = (str: string) => {
    let text = str.toLowerCase();
    let fullList = selectedCatArray;
    // search for given text in list
    let filteredList = fullList.filter((item) => {
      if(item.status.toLowerCase().match(text))
        return item;
    })
    if(filteredList.length > 0){
      this.setState ({
        dataSource: filteredList,
        search:str,
     })
    }
  }

  renderSeparatorView = () => {
    return (
      <View style={{
          height: 1,
          width: "100%",
          backgroundColor: "#CEDCCE",
        }}
      />
    );
  };

  chooseImage(index,item) {
    // set image as per selected category
    if(selectedCat == 'all'){
      if (index%3 == 0){
        return this.state.selectedCellIndex === index ? imgASelected : imgA
      }
      else if (index%3 == 1){
        return this.state.selectedCellIndex === index ? imgBSelected : imgB
      }
      else if (index%3 == 2){
        return this.state.selectedCellIndex === index ? imgCSelected : imgC
      }
    }else if(selectedCat == 'A'){
      return this.state.selectedCellIndex === index ? imgASelected : imgA
    }else if(selectedCat == 'B'){
      return this.state.selectedCellIndex === index ? imgBSelected : imgB
    }else if(selectedCat == 'C'){
      return this.state.selectedCellIndex === index ? imgCSelected : imgC
    }
  }

  render(){
    totalBalance = Math.floor(totalBalance * 100) / 100;
    return (
      <View style= {styles.container}>
        <View style= {styles.topContainer}>
          <View style= {{ justifyContent: "center", width: 40, height: 40, backgroundColor: "#31C5C3", borderRadius: 20}}>
            <Image source= {help} style= {styles.topIcons} />
          </View>
          <View >
            <Image source= {appLogo} style= {styles.topIcons2} />
          </View>
          <View >
            <Image source= {bell} style= {styles.topIcons} />
          </View>
        </View>
        <View style= {styles.middleContainer}>
          <View style= {styles.middleContainer2}>
          </View>
          <View style= {{flex:0.23, flexDirection: "row", justifyContent: "flex-start", marginLeft: 60, marginBottom: 0, marginRight: 40}}>
            <View style= {{alignSelf: "center", height: 10, width: 10, borderRadius: 5, backgroundColor: "#31C5C3", marginTop: -5}}>
            </View>
            <Text style= {{marginLeft: 10, alignSelf: "center",  color: "#898989", fontSize: 20, fontFamily:'GothamRounded-Book', fontWeight:'500'}}>
              Your Portfolio
            </Text>
          </View>
          <View style= {{flex:0.33, flexDirection: "row", backgroundColor: "white", alignItems: "center", paddingLeft: 45}}>
              <Image source= {this.state.selectedCatImage} style= {{  width: 40, height: 40, resizeMode: "contain", alignSelf: "center", marginTop: -5}} />
              <Text style= {{color: "#3AD1BF", paddingRight:20, paddingTop:10, paddingLeft: 10, fontSize: 55, justifyContent: "flex-start", fontWeight: "bold", fontFamily:'GothamRounded-Book'}}>
                {totalBalance.toLocaleString('en')}
              </Text>
          </View>
          <View style= {{flex:0.43, flexDirection: "row"}}>
            <View style= {styles.categoryContainer}>
              <TouchableOpacity onPress={this.catAPressed.bind()} style={ this.state.categoryIndex == 1 ? styles.selectedCategoryTouch : styles.categoryTouch}>
                <Text style={ this.state.categoryIndex == 1 ? styles.selectedCategoryText : styles.categoryText}>
                  {arrayCategoryKeys[0]}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.catBPressed.bind()} style={ this.state.categoryIndex == 2 ? styles.selectedCategoryTouch : styles.categoryTouch}>
                <Text style={ this.state.categoryIndex == 2 ? styles.selectedCategoryText : styles.categoryText}>
                  {arrayCategoryKeys[1]}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.catCPressed.bind()} style={ this.state.categoryIndex == 3 ? styles.selectedCategoryTouch : styles.categoryTouch}>
                <Text style={ this.state.categoryIndex == 3 ? styles.selectedCategoryText : styles.categoryText}>
                  {arrayCategoryKeys[2]}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.catAllPressed.bind()} style={ this.state.categoryIndex == 4 ? styles.selectedCategoryTouch : styles.categoryTouch}>
                <Text style={ this.state.categoryIndex == 4 ? styles.selectedCategoryText : styles.categoryText}>
                  ALL
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style= {styles.bottomContainer}>
          <View style= {{flexDirection: "row", height: 50, marginLeft: 0, marginBottom: 20, marginRight: 30, marginTop: 20, justifyContent: "space-between"}}>
            {!this.state.isSearchMode && <View style= {{alignSelf: "center", height: 10, width: 10, borderRadius: 5, backgroundColor: "#31C5C3", marginTop: -5, marginLeft: 60}}>
            </View>
           }
            {this.state.isSearchMode ?
              <TextInput
                style = {styles.searchbar}
                value= {this.state.search}
                onChangeText= {this.handleSearch.bind()}
                placeholder= "Search Value..."
                placeholderTextColor= "#B3B3B3"
                /> :
                <Text style= {{marginLeft: 80, alignSelf: "center",  color: "#898989", fontSize: 20, fontWeight:'500', position: "absolute", fontFamily:'GothamRounded-Book'}}>
                  Recent Values
                </Text>
            }
            <TouchableOpacity onPress={this.searchPressed.bind()} style={ this.state.isSearchMode ? {borderRadius:5, alignSelf: "center", width: 40, backgroundColor: "white"} : {borderRadius:5, alignSelf: "center", width: 40, backgroundColor: "#3AD1BF"}}>
              <Image source= {this.state.isSearchMode ? search : searchWhite} style= {styles.searchIcon} />
            </TouchableOpacity>
          </View>
          <View style= {styles.filterContainer}>
            <TouchableOpacity onPress={this.filterAllPressed.bind()} style={this.state.filterIndex == 4 ? styles.selectedFilterTouch : styles.filterTouch}>
              <Text style={this.state.filterIndex == 4 ? styles.selectedFilterText : styles.filterText}>
                {this.state.filter4Text}
              </Text>
            </TouchableOpacity>
           <TouchableOpacity onPress={this.filter1Pressed.bind()} style={this.state.filterIndex == 1 ? styles.selectedFilterTouch : styles.filterTouch}>
             <Text style={this.state.filterIndex == 1 ? styles.selectedFilterText : styles.filterText}>
               {this.state.filter1Text}
             </Text>
           </TouchableOpacity>
           <TouchableOpacity onPress={this.filter2Pressed.bind()} style={this.state.filterIndex == 2 ? styles.selectedFilterTouch : styles.filterTouch}>
             <Text style={this.state.filterIndex == 2 ? styles.selectedFilterText : styles.filterText}>
               {this.state.filter2Text}
             </Text>
           </TouchableOpacity>
           <TouchableOpacity onPress={this.filter3Pressed.bind()} style={this.state.filterIndex == 3 ? styles.selectedFilterTouch : styles.filterTouch}>
             <Text style={this.state.filterIndex == 3 ? styles.selectedFilterText : styles.filterText}>
               {this.state.filter3Text}
             </Text>
           </TouchableOpacity>
          </View>
          <FlatList
            data={this.state.dataSource}
            showsVerticalScrollIndicator={false}
            renderItem={({item,index}) =>
            <TouchableOpacity key={index} onPress={() => this.handleCellSelection(index)} style={ this.state.selectedCellIndex === index ? styles.selectedfFatview : styles.flatview}>
              <Image source= {this.chooseImage(index,item)} style= {{  width: 30, height: 30, resizeMode: "contain", alignSelf: "center", marginLeft: 40}} />
              <Text style={ this.state.selectedCellIndex === index ? styles.selectedStatusText : styles.statusText}>{item.status}</Text>

              <Text style={this.state.selectedCellIndex === index ? styles.selectedValueText : styles.valueText}> {item.balance} </Text>
            </TouchableOpacity>
            }
            ItemSeparatorComponent={this.renderSeparatorView}
            keyExtractor={(item, index)=> index.toString()}
          />
        </View>
      </View>
      );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-between"
  },
  topContainer: {
    flex: 0.10,
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-between",
    margin: 20,
  },
  middleContainer: {
    flex: 0.35,
    justifyContent: "space-between",
    backgroundColor: 'white',
    height: 40,
    shadowColor: "#898989",
    shadowOpacity: 0.2,
    shadowRadius: 1,
    shadowOffset: {
      height: 2,
      width: 0,
    },
  },
  middleContainer2: {
    flex: 0.15,
    justifyContent: "space-between",
    backgroundColor: 'white',
    height: 40,
    shadowColor: "#898989",
    shadowOpacity: 0.2,
    shadowRadius: 1,
    shadowOffset: {
      height: -1,
      width: 0,
    },
  },
  bottomContainer: {
    flex: 0.55,
  },
  topIcons: {
    width: 30,
    height: 30,
    resizeMode: "contain",
    alignSelf: "center"
  },
  topIcons2: {
    width: 60,
    height: 60,
    marginBottom: -10,
    resizeMode: "contain",
    alignSelf: "center"
  },
  categoryContainer: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "#F2F2F2",
    justifyContent: "center",
    margin: 20,
    paddingLeft:10,
    paddingRight:10,
    paddingTop: 5,
    paddingBottom: 5,
    height: 35,
    borderRadius: 20,
    alignSelf: "center"
  },
  categoryTouch: {
    borderRadius: 20,
    width: "auto",
    justifyContent: "center",
    padding:5,
  },
  selectedCategoryTouch: {
    borderRadius: 20,
    width: "auto",
    justifyContent: "center",
    alignItems:'center',
    padding:5,
    backgroundColor: "#3AD1BF"
  },
  filterTouch: {
    borderRadius: 20,
    width: "25%",
    justifyContent: "center",
    //backgroundColor: "#3AD1BF"
  },
  selectedFilterTouch: {
    borderRadius: 20,
    width: "25%",
    justifyContent: "center",
    backgroundColor: "#3AD1BF"
  },
  categoryText: {
    fontSize: 9,
    alignSelf: "center",
    color: "#3AD1BF",
    fontFamily:'GothamRounded-Book',
    textTransform: 'uppercase',
    fontWeight:'500',
    paddingTop:3,
    padding:5,
  },
  selectedCategoryText: {
    fontSize: 9,
    color: "white",
    fontFamily:'GothamRounded-Book',
    textTransform: 'uppercase',
    fontWeight:'500',
    paddingTop:3,
    padding:5,
  },
  filterContainer: {
    height: 40,
    margin: 20,
    marginTop: 0,
    flexDirection: "row",
    backgroundColor: "#F2F2F2",
    borderRadius:20,
    justifyContent: "center",
    paddingLeft:10,
    paddingRight:10,
    paddingTop: 5,
    paddingBottom: 5,
  },
  filterText: {
    fontSize: 10,
    alignSelf: "center",
    color: "#35C8CC",
    fontFamily:'GothamRounded-Book',
    fontWeight:'500'
  },
  selectedFilterText: {
    fontSize: 10,
    alignSelf: "center",
    color: "white",
    fontFamily:'GothamRounded-Book',
    fontWeight:'500',
    paddingTop:2,
  },
  searchIcon: {
    width: 20,
    resizeMode: "contain",
    alignSelf: "center"
  },
  searchbar: {
    borderRadius: 6,
    marginLeft: 20,
    marginRight: 20,
    width: "80%",
    paddingLeft: 20,
    fontWeight:'500',
    backgroundColor: 'white',
    height: 40,
    shadowColor: "#898989",
    shadowOpacity: 0.2,
    shadowRadius: 1,
    shadowOffset: {
      height: 1,
      width: 0,
    },
  },
  flatview: {
   justifyContent: 'space-between',
   paddingTop: 15,
   paddingBottom: 15,
   borderColor: "black",
   borderRadius: 2,
   flexDirection: "row",
  },
  selectedfFatview: {
   justifyContent: 'space-between',
   paddingTop: 15,
   paddingBottom: 15,
   borderColor: "black",
   borderRadius: 2,
   flexDirection: "row",
   backgroundColor: "#3AD1BF"
 },
 selectedStatusText: {
   marginLeft: 100, alignSelf: "center",  color: "white", fontSize: 16, position: "absolute", fontFamily:'GothamRounded-Book',textTransform:'capitalize'
 },
 statusText: {
   marginLeft: 100,textTransform:'capitalize', alignSelf: "center",  color: "#898989", fontSize: 16, position: "absolute", fontFamily:'GothamRounded-Book'
 },
 selectedValueText: {
   marginRight: 20, alignSelf: "center", fontFamily:'GothamRounded-Book', color: "white", fontWeight:'500'
 },
 valueText: {
    marginRight: 20, alignSelf: "center", color: "#898989",fontFamily:'GothamRounded-Book', fontWeight:'500'
 },

});
